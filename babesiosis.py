'''
                                 ESP Health
                        Notifiable Diseases Framework
                         Babesiosis Case Generator


@author: Jeff Andre <jandre@commoninf.com>
@organization: commonwealth informatics https://www.commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2019 Commonwealth Informatics, Inc.
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.  
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
from dateutil.relativedelta import relativedelta
import datetime

from django.db.models import F

from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import LabResultPositiveHeuristic, PrescriptionHeuristic, DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.static.models import DrugSynonym
from ESP.conf.models import LabTestMap


class Babesiosis(DiseaseDefinition):
    '''
    Babesiosis
    '''
    
    conditions = ['babesiosis']
    
    uri = 'urn:x-esphealth:disease:channing:babesiosis:v1'
    
    short_name = 'babesiosis'
    
    test_name_search_strings = ['babesiosis','babesia', 'bloodsmear']

    timespan_heuristics = []

    recurrence = 365

    @property
    def event_heuristics(self):
        heuristic_list = []
        
        """ Diagnosis Codes
        """
        heuristic_list.append( DiagnosisHeuristic(
            name = 'babesiosis',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='088.82', type='icd9'),
                Dx_CodeQuery(starts_with='B60.0', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'fever_unspecified',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.60', type='icd9'),
                Dx_CodeQuery(starts_with='R50.9', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'chills',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.64', type='icd9'),
                Dx_CodeQuery(starts_with='R68.83', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'sweats',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.8', type='icd9'),
                Dx_CodeQuery(starts_with='R61', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'headache',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='784.0', type='icd9'),
                Dx_CodeQuery(starts_with='R51', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'myalgia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='729.1', type='icd9'),
                Dx_CodeQuery(starts_with='M79.1', type='icd10'),
                Dx_CodeQuery(starts_with='M79.10', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'arthralgia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='719.40', type='icd9'),
                Dx_CodeQuery(starts_with='M25.50', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'anemia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='285.9', type='icd9'),
                Dx_CodeQuery(starts_with='D64.9', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'thrombocytopenia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='287.5', type='icd9'),
                Dx_CodeQuery(starts_with='D69.6', type='icd10'),
                ]
            ))

        """ Prescriptions
        """
        for drug in [
            'atovaquone',
            'azithromycin',
            'clindamycin',
            'quinine',
            ]:
         
            heuristic_list.append( PrescriptionHeuristic(
                name = drug,
                drugs = DrugSynonym.generics_plus_synonyms([drug]),
                ))
        """ Lab Results
        """
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_microti_pcr',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_spp_pcr',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_naat',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'tick_bloodsmear',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_igg_10',
            titer_dilution = 10,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_igg_16',
            titer_dilution = 16,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_igg_64',
            titer_dilution = 64,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'babesiosis_igg_256',
            titer_dilution = 256,
            ))

        return heuristic_list


    def generate(self):
        LabTestMap.create_update_dummy_lab(self.conditions[0], 'MDPH-319', 'MDPH-R490')
        log.info('Generating cases of %s' % self.short_name)

        """ Criteria #1: Diagnosis for Babesiosis and prescriptions (on the same day) 
            for one of the following combinations of antibiotics:
               a. atovaquone and azithromycin
               b. clindamycin and quinine
            The diagnosis code and prescriptions occur within 14 days of one another.
        """
        time_period = 14
        dx_event_names = ['dx:babesiosis']
        rx_event_names = [
                'rx:atovaquone',
                'rx:azithromycin',
                'rx:clindamycin',
                'rx:quinine'
                ]
        dxrx_event1_qs = Event.objects.filter(
            name__in = dx_event_names,
            patient__event__name__in = rx_event_names[:1],
            patient__event__date__gte = (F('date') - datetime.timedelta(days=time_period)),
            patient__event__date__lte = (F('date') + datetime.timedelta(days=time_period)),
            )
        dxrx_event2_qs = Event.objects.filter(
            name__in = dx_event_names,
            patient__event__name__in = rx_event_names[2:],
            patient__event__date__gte = (F('date') - datetime.timedelta(days=time_period)),
            patient__event__date__lte = (F('date') + datetime.timedelta(days=time_period)),
            )
        dxrx_event_qs = dxrx_event1_qs | dxrx_event2_qs

        """
        Criteria #2: Positive Babesia blood smear test
        """
        lx_blood_smear_event_names = [
                'lx:tick_bloodsmear:positive',
                ]
        lx_blood_smear_event_qs = Event.objects.filter(
                name__in = lx_blood_smear_event_names,
                )
        """
        Criteria #3: Positive Babesia microti PCR test
        """
        lx_microti_pcr_event_names = [
                'lx:babesiosis_microti_pcr:positive',
                ]
        lx_microti_pcr_event_qs = Event.objects.filter(
                name__in = lx_microti_pcr_event_names,
                )
        """
        Criteria #4: Positive Babesia spp. or nucleic acid amplification test
        """
        lx_pcr_naat_event_names = [
                'lx:babesiosis_spp_pcr:positive',
                'lx:babesiosis_naat:positive',
                ]
        lx_pcr_naat_event_qs = Event.objects.filter(
                name__in = lx_pcr_naat_event_names,
                )
        """
        Criteria #5: Positive Babesia microti or duncani IgG test
        """
        lx_igg_event_names = [
                'lx:babesiosis_igg_10:positive',
                'lx:babesiosis_igg_16:positive',
                'lx:babesiosis_igg_64:positive',
                'lx:babesiosis_igg_256:positive',
                ]
        lx_igg_event_qs = Event.objects.filter(
                name__in = lx_igg_event_names,
                )
        """Combined Criteria
        """
        combined_criteria_qs = (dxrx_event_qs | lx_microti_pcr_event_qs | 
                                lx_pcr_naat_event_qs | lx_igg_event_qs | 
                                lx_blood_smear_event_qs)
        combined_criteria_qs = combined_criteria_qs.exclude(
                                                    case__condition=self.conditions[0], 
                                                    ).order_by('date')

        """Case Detection
        """
        counter = 0
        for this_event in combined_criteria_qs:
            # Case criteria
            new_criteria = ''
            related_rx_events = None
            if this_event.name in dx_event_names:
                # Criteria #1 a.
                rx1_events = Event.objects.filter(
                            patient = this_event.patient,
                            name = rx_event_names[0],
                            patient__event__name = rx_event_names[1],
                            date__gte = (this_event.date - datetime.timedelta(days=time_period)),
                            date__lte = (this_event.date + datetime.timedelta(days=time_period)),
                            patient__event__date = F('date'),
                            )
                # Criteria #1 b. 
                rx2_events = Event.objects.filter(
                            patient = this_event.patient,
                            name = rx_event_names[2],
                            patient__event__name = rx_event_names[3],
                            date__gte = (this_event.date - datetime.timedelta(days=time_period)),
                            date__lte = (this_event.date + datetime.timedelta(days=time_period)),
                            patient__event__date = F('date'),
                            )
                if rx1_events or rx2_events:

                    new_criteria = 'Criteria #1: babesiosis diagnosis and prescription for combination of antibiotics (atovaquone and azithromycin, or clindamycin and quinine) within 14 days of diagnosis'
                    related_rx_events = rx1_events | rx2_events
                    # Get 2nd combo antibiotic events
                    if rx1_events:
                        related_rx_events |= Event.objects.filter(
                            patient = this_event.patient,
                            name = rx_event_names[1],
                            patient__event__name = rx_event_names[0],
                            date__gte = (this_event.date - datetime.timedelta(days=time_period)),
                            date__lte = (this_event.date + datetime.timedelta(days=time_period)),
                            patient__event__date = F('date'),
                            )
                    if rx2_events:
                        related_rx_events |= Event.objects.filter(
                            patient = this_event.patient,
                            name = rx_event_names[3],
                            patient__event__name = rx_event_names[2],
                            date__gte = (this_event.date - datetime.timedelta(days=time_period)),
                            date__lte = (this_event.date + datetime.timedelta(days=time_period)),
                            patient__event__date = F('date'),
                            )

            elif this_event.name in lx_blood_smear_event_names:
                new_criteria = 'Criteria #2: positive Babesia stained blood smear test'
            elif this_event.name in lx_microti_pcr_event_names:
                new_criteria = 'Criteria #3: positive Babesia microti PCR test'
            elif this_event.name in lx_pcr_naat_event_names:
                new_criteria = 'Criteria #4: positive Babesia spp. PCR or nucleic acid amplification test'
            elif this_event.name in lx_igg_event_names:
                new_criteria = 'Criteria #5: positive Babesia microti or duncani IgG IFA test'
            # create case
            if new_criteria:
                created, new_case = self._create_case_from_event_obj(
                    condition=self.conditions[0],
                    criteria=new_criteria,
                    recurrence_interval=self.recurrence,
                    event_obj=this_event,
                    relevant_event_qs=related_rx_events
                )
                if created:
                    log.debug('Created new babesiosis case: %s' % new_case)
                    counter += 1

        log.debug('Generating %s new cases of babesiosis' % counter)
        return counter # count of new cases

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
        }

        return reportable_fields.get(report_field, None)
    
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------


def event_heuristics():
    return Babesiosis().event_heuristics

def disease_definitions():
    return [Babesiosis()]

