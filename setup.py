'''
                                  ESP Health
                                 Babesiosis
                             Packaging Information


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics https://commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2019
@license: LGPL 3.0 http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_babesiosis',
    version='1.2',
    author='Jeff Andre',
    author_email='jandre@commoninf.com',
    description='Babesiosis disease definition module for ESP Health application',
    license='LGPLv3',
    keywords='Babesiosis algorithm disease surveillance public health epidemiology',
    url='https://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = babesiosis:disease_definitions
        event_heuristics = babesiosis:event_heuristics
    '''
)
